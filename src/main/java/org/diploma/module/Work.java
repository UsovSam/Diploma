package org.diploma.module;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Work {

    private int id;
    private String name;
    private int workers;
    private int amount;
//    private String units;
    private List<String> prevWork;

}
