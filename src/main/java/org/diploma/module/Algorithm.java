package org.diploma.module;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Algorithm {

    List<Work> works;
    List<Work> usedWorks;
    int workers;
    int[] resources;
    List<List<Work>> days = new ArrayList<>();
    private Work el;

    public List<List<Work>> calculate() {

        Integer tMax = works.stream()
                .flatMapToInt(work -> IntStream.of(work.getAmount()))
                .reduce((identity, val) -> {
                    return identity + val;
                })
                .getAsInt();

        System.out.println("Tmax " + tMax);

        resources = new int[tMax];
        days = new ArrayList<>(tMax);
        usedWorks = new ArrayList<>();
        for (int i = 0; i < tMax; i++) {
            resources[i] = workers;
            days.add(i, new ArrayList<>());
        }

        List EL = works.stream().filter(work -> work.getPrevWork().isEmpty()).collect(Collectors.toList());
        System.out.println(EL);

        while (EL.size() > 0) {
            //TODO брать элемент исходя из предыдущего опыта, если их больше 1
            Work el = (Work) EL.remove(0);
//            System.out.println("work - " + el);
            int timeStart = getTimeToStart(el);
            System.out.println("time to start - " + timeStart);

            for (int i = 0; i < el.getAmount(); i++) {
                resources[timeStart + i] -= el.getWorkers();
                List buf = days.get(timeStart + i);
                buf.add(el);
                days.set(timeStart + i, buf);
            }

//            System.out.println("days - " + days);
//            System.out.println("resources - " + resources.toString());
            usedWorks.add(el);
            EL = getListNextWork(el.getId());
            System.out.println(EL);
          /*  EL = works.stream()
                    .filter(work -> (work.getPrevWork().contains(el.getId()) || work.getPrevWork().contains(-1))
                            && work.getId() != el.getId())
                    .collect(Collectors.toList());*/

        }


        System.out.println("----------------------------------------------------");
        System.out.println(usedWorks);
        System.out.println(days.stream().filter(item -> item.size() > 0).count());
        int i = 0;
        for (List day : days) {
            i++;
            System.out.println("day #" + i);
            day.forEach(System.out::println);
        }
        System.out.println("days - " + days);
        return days;
    }

    private List getListNextWork(int lastId) {
        //get not used;
        List<Work> l = works.stream()
                .filter(work -> {
                    AtomicBoolean isUsed = new AtomicBoolean(false);
                    //check if used
                    usedWorks.forEach(item -> {
                        if (work.getId() == item.getId()) {
                            isUsed.set(true);
                        }
                    });

                    if (isUsed.get()) {
                        return false;
                    } else {
                        return true;
                    }
                })
                .collect(Collectors.toList());

        List<Work> d = l.stream()
                .filter(work -> {
                    AtomicBoolean hasPrev = new AtomicBoolean(false);
                    usedWorks.forEach(item -> {
                        if (work.getPrevWork().contains(String.valueOf(item.getId()))) {
                            hasPrev.set(true);
                        }
                    });
                    return hasPrev.get();
                })
                .collect(Collectors.toList());


        return d;
    }

    private int getTimeToStart(Work work) {
        boolean flag = true;
        for (int i = 0; i < resources.length; i++) {
            if (resources[i] >= work.getWorkers()) {
                flag = true;
                int offset = 0; // last work from prev
                for (int k = 0; k < days.size(); k++) {
                    if (days.get(k).stream().filter(item -> work.getPrevWork().contains(String.valueOf(item.getId()))).count() > 0) {
                        offset = k;
                    }
                }
                if(offset > i){
                    continue;
                }
                //смотрим на весь нужный нам период
                for (int j = i + offset; j < i + offset + work.getAmount() && j < resources.length; j++) {
                    //TODO добавить проверку на предшественников
                    if (resources[j] < work.getWorkers() || isPresentPrev(j, work)) {
                        flag = false;
                    }
                }
                if (flag) {
                    return i;
                }
            }
        }

        return resources.length;
    }

    private boolean isPresentPrev(int j, Work work) {
        return days.get(j).stream()
                .filter(
                        item -> work.getPrevWork()
                                .contains(String.valueOf(item.getId())))
                .count() > 0;
    }


}
