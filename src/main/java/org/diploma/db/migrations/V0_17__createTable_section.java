package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_17__createTable_section implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `section` (" +
                "  `id_section` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_section` VARCHAR(250) NOT NULL," +
                "  `code_section` VARCHAR(50) NOT NULL," +
                "  `collection_id_collection` BIGINT NOT NULL," +
                "  PRIMARY KEY (`id_section`)," +
                "  INDEX `fk_section_collection1_idx` (`collection_id_collection` ASC)," +
                "  CONSTRAINT `fk_section_collection1`" +
                "    FOREIGN KEY (`collection_id_collection`)" +
                "    REFERENCES `collection` (`id_collection`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
           "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
