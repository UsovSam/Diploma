package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_04__createTable_projectHasUser implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `projects_has_users` (" +
                "  `projects_id_project` BIGINT NOT NULL," +
                "  `users_id_user` BIGINT NOT NULL," +
                "  PRIMARY KEY (`projects_id_project`, `users_id_user`)," +
                "  INDEX `fk_projects_has_users_users1_idx` (`users_id_user` ASC)," +
                "  INDEX `fk_projects_has_users_projects1_idx` (`projects_id_project` ASC)," +
                "  CONSTRAINT `fk_projects_has_users_projects1`" +
                "    FOREIGN KEY (`projects_id_project`)" +
                "    REFERENCES `projects` (`id_project`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_projects_has_users_users1`" +
                "    FOREIGN KEY (`users_id_user`)" +
                "    REFERENCES `users` (`id_user`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }
}
