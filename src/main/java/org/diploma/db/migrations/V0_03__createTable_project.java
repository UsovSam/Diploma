package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_03__createTable_project implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `projects` (" +
                "  `id_project` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_project` VARCHAR(250) NOT NULL," +
                "  `budjet_project` DECIMAL(10,2) NULL," +
                "  `date_start` DATETIME NOT NULL," +
                "  `date_end` DATETIME NOT NULL," +
                "  `document` BLOB NULL," +
                "  `comment` VARCHAR(500) NULL," +
                "  PRIMARY KEY (`id_project`))" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");


    }
}
