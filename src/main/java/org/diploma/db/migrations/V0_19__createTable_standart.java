package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_19__createTable_standart implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `standart` (" +
                "  `id_standart` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_standart` VARCHAR(250) NOT NULL," +
                "  `code_standart` VARCHAR(50) NOT NULL," +
                "  `group_id_group` BIGINT NOT NULL," +
                "  PRIMARY KEY (`id_standart`)," +
                "  INDEX `fk_standart_group1_idx` (`group_id_group` ASC)," +
                "  CONSTRAINT `fk_standart_group1`" +
                "    FOREIGN KEY (`group_id_group`)" +
                "    REFERENCES `group` (`id_group`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
