package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_01__createTable_roles implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `roles` (" +
                "  `id_role` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `role_name` VARCHAR(250) NOT NULL," +
                "  `role_alias` VARCHAR(500) NOT NULL," +
                "  PRIMARY KEY (`id_role`))" +
                "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");

        jdbcTemplate.execute("INSERT INTO roles(role_name, role_alias) VALUES ('ROLE_ADMIN','Администратор')");
        jdbcTemplate.execute("INSERT INTO roles(role_name, role_alias) VALUES ('ROLE_USER','Пользователь')");
    }
}
