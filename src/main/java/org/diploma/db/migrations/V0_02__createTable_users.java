package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_02__createTable_users implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `users` (" +
                "  `id_user` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `login` VARCHAR(200) NOT NULL," +
                "  `password` VARCHAR(500) NOT NULL," +
                "  `name` VARCHAR(200) NOT NULL," +
                "  `surname` VARCHAR(200) NOT NULL," +
                "  `roles_id_role` BIGINT NOT NULL," +
                "  PRIMARY KEY (`id_user`)," +
                "  UNIQUE INDEX `login_UNIQUE` (`login` ASC)," +
                "  INDEX `fk_users_roles1_idx` (`roles_id_role` ASC)," +
                "  CONSTRAINT `fk_users_roles1`" +
                "    FOREIGN KEY (`roles_id_role`)" +
                "    REFERENCES `roles` (`id_role`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");

        jdbcTemplate.execute(" INSERT INTO users(login, password, name, surname, roles_id_role) VALUES ('root','$2a$10$CIzbjeNbW9DDlDcBm60x/.VJniVYpmtviNufURMhVUq3ox44iaHcS','root','root',1)");
    }
}
