package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_13__createTable_work implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `work` (" +
                "  `id_work` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `count` DECIMAL(10,2) NOT NULL," +
                "  `price` DECIMAL(10,2) NOT NULL," +
                "  `section_smeta_id_section` BIGINT NOT NULL," +
                "  `directory_resource_id_directiory_resource` BIGINT NOT NULL," +
                "  PRIMARY KEY (`id_work`)," +
                "  INDEX `fk_work_section_smeta1_idx` (`section_smeta_id_section` ASC)," +
                "  INDEX `fk_work_directory_resource1_idx` (`directory_resource_id_directiory_resource` ASC)," +
                "  CONSTRAINT `fk_work_section_smeta1`" +
                "    FOREIGN KEY (`section_smeta_id_section`)" +
                "    REFERENCES `section_smeta` (`id_section`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_work_directory_resource1`" +
                "    FOREIGN KEY (`directory_resource_id_directiory_resource`)" +
                "    REFERENCES `directory_resource` (`id_directiory_resource`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
