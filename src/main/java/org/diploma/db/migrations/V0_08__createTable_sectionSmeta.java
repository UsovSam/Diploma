package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_08__createTable_sectionSmeta implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `section_smeta` (\n" +
                "  `id_section` BIGINT NOT NULL AUTO_INCREMENT,\n" +
                "  `name_section` VARCHAR(250) NOT NULL,\n" +
                "  `complexity_section` DECIMAL(10,2) NULL,\n" +
                "  `rank_section` DECIMAL(10,2) NULL,\n" +
                "  `material_cost_section` DECIMAL(10,2) NULL,\n" +
                "  `sum_section` DECIMAL(10,2) NULL,\n" +
                "  `smeta_id_smeta` BIGINT NOT NULL,\n" +
                "  PRIMARY KEY (`id_section`),\n" +
                "  INDEX `fk_section_smeta_smeta1_idx` (`smeta_id_smeta` ASC),\n" +
                "  CONSTRAINT `fk_section_smeta_smeta1`\n" +
                "    FOREIGN KEY (`smeta_id_smeta`)\n" +
                "    REFERENCES `smeta` (`id_smeta`)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION)\n" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
