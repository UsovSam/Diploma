package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_09__createTable_directoryMachine implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `directory_machine` (" +
                "  `id_directory_machine` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_directory_machine` VARCHAR(500) NOT NULL," +
                "  `units_id_unit` BIGINT NOT NULL," +
                "  PRIMARY KEY (`id_directory_machine`)," +
                "  INDEX `fk_directory_machine_units1_idx` (`units_id_unit` ASC)," +
                "  CONSTRAINT `fk_directory_machine_units1`" +
                "    FOREIGN KEY (`units_id_unit`)" +
                "    REFERENCES `units` (`id_unit`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
