package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_20__createTable_generalizedWork implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `generalized_works` (" +
                "  `id_generelized` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_generalized` VARCHAR(250) NOT NULL," +
                "  `value_generalized` DECIMAL(10,2) NOT NULL," +
                "  `count_day` DECIMAL(10,2) NOT NULL," +
                "  `count_workers` DECIMAL(10,2) NOT NULL," +
                "  `prev_work`VARCHAR(50) NULL," +
                "  `list_works_id_list` BIGINT NOT NULL," +
                "  `units_id_unit` BIGINT NOT NULL," +
                "  `work_id_work` BIGINT NULL," +
                "  `machine_id_machine` BIGINT NULL," +
                "  `material_id_material` BIGINT NULL," +
                "  `standart_id_standart` BIGINT NULL," +
                "  PRIMARY KEY (`id_generelized`)," +
                "  INDEX `fk_generalized_works_list_works1_idx` (`list_works_id_list` ASC)," +
                "  INDEX `fk_generalized_works_units1_idx` (`units_id_unit` ASC)," +
                "  INDEX `fk_generalized_works_work1_idx` (`work_id_work` ASC)," +
                "  INDEX `fk_generalized_works_machine1_idx` (`machine_id_machine` ASC)," +
                "  INDEX `fk_generalized_works_material1_idx` (`material_id_material` ASC)," +
                "  INDEX `fk_generalized_works_standart1_idx` (`standart_id_standart` ASC)," +
                "  CONSTRAINT `fk_generalized_works_list_works1`" +
                "    FOREIGN KEY (`list_works_id_list`)" +
                "    REFERENCES `list_works` (`id_list`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_generalized_works_units1`" +
                "    FOREIGN KEY (`units_id_unit`)" +
                "    REFERENCES `units` (`id_unit`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_generalized_works_work1`" +
                "    FOREIGN KEY (`work_id_work`)" +
                "    REFERENCES `work` (`id_work`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_generalized_works_machine1`" +
                "    FOREIGN KEY (`machine_id_machine`)" +
                "    REFERENCES `machine` (`id_machine`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_generalized_works_material1`" +
                "    FOREIGN KEY (`material_id_material`)" +
                "    REFERENCES `material` (`id_material`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_generalized_works_standart1`" +
                "    FOREIGN KEY (`standart_id_standart`)" +
                "    REFERENCES `standart` (`id_standart`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
