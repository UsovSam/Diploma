package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_12__createTable_directoryResource implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `directory_resource` (" +
                "  `id_directiory_resource` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `type_resource` INT NOT NULL," +
                "  `code_resource` VARCHAR(50) NOT NULL," +
                "  `price_resource` DECIMAL(10,2) NOT NULL," +
                "  `directory_machine_id_directory_machine` BIGINT NULL," +
                "  `directory_characteristic_id_directory_characteristic` BIGINT NULL," +
                "  `directory_material_id_directory_material` BIGINT NULL," +
                "  PRIMARY KEY (`id_directiory_resource`)," +
                "  INDEX `fk_directory_resource_directory_machine1_idx` (`directory_machine_id_directory_machine` ASC)," +
                "  INDEX `fk_directory_resource_directory_characteristic1_idx` (`directory_characteristic_id_directory_characteristic` ASC)," +
                "  INDEX `fk_directory_resource_directory_material1_idx` (`directory_material_id_directory_material` ASC)," +
                "  CONSTRAINT `fk_directory_resource_directory_machine1`" +
                "    FOREIGN KEY (`directory_machine_id_directory_machine`)" +
                "    REFERENCES `directory_machine` (`id_directory_machine`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_directory_resource_directory_characteristic1`" +
                "    FOREIGN KEY (`directory_characteristic_id_directory_characteristic`)" +
                "    REFERENCES `directory_characteristic` (`id_directory_characteristic`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_directory_resource_directory_material1`" +
                "    FOREIGN KEY (`directory_material_id_directory_material`)" +
                "    REFERENCES `directory_material` (`id_directory_material`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
