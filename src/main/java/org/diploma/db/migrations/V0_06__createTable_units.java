package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_06__createTable_units implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `units` (" +
                "  `id_unit` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_unit` VARCHAR(50) NOT NULL," +
                "  PRIMARY KEY (`id_unit`))" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }
}
