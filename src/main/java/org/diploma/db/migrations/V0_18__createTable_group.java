package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_18__createTable_group implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `group` (" +
                "  `id_group` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_group` VARCHAR(250) NOT NULL," +
                "  `code_group` VARCHAR(50) NOT NULL," +
                "  `measure_group` DECIMAL(10,0) NOT NULL," +
                "  `section_id_section` BIGINT NOT NULL," +
                "  `units_id_unit` BIGINT NOT NULL," +
                "  PRIMARY KEY (`id_group`)," +
                "  INDEX `fk_group_section1_idx` (`section_id_section` ASC)," +
                "  INDEX `fk_group_units1_idx` (`units_id_unit` ASC)," +
                "  CONSTRAINT `fk_group_section1`" +
                "    FOREIGN KEY (`section_id_section`)" +
                "    REFERENCES `section` (`id_section`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_group_units1`" +
                "    FOREIGN KEY (`units_id_unit`)" +
                "    REFERENCES `units` (`id_unit`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");

    }


}
