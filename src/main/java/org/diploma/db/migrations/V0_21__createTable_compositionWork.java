package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_21__createTable_compositionWork implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `composition_work` (" +
                "  `id_composition_work` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_composition_work` VARCHAR(500) NOT NULL," +
                "  `group_id_group` BIGINT NOT NULL," +
                "  PRIMARY KEY (`id_composition_work`)," +
                "  INDEX `fk_composition_work_group1_idx` (`group_id_group` ASC)," +
                "  CONSTRAINT `fk_composition_work_group1`" +
                "    FOREIGN KEY (`group_id_group`)" +
                "    REFERENCES `diploma`.`group` (`id_group`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
