package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_22__createTable_resource implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `resource` (" +
                "  `id_resource` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `value_resource` DECIMAL(10,2) NOT NULL," +
                "  `directory_resource_id_directiory_resource` BIGINT NOT NULL," +
                "  PRIMARY KEY (`id_resource`)," +
                "  INDEX `fk_resource_directory_resource1_idx` (`directory_resource_id_directiory_resource` ASC)," +
                "  CONSTRAINT `fk_resource_directory_resource1`" +
                "    FOREIGN KEY (`directory_resource_id_directiory_resource`)" +
                "    REFERENCES `directory_resource` (`id_directiory_resource`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
