package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_23__createTable_standartHasResource implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `standart_has_resource` (" +
                "  `standart_id_standart` BIGINT NOT NULL," +
                "  `resource_id_resource` BIGINT NOT NULL," +
                "  PRIMARY KEY (`standart_id_standart`, `resource_id_resource`)," +
                "  INDEX `fk_standart_has_resource_resource1_idx` (`resource_id_resource` ASC)," +
                "  INDEX `fk_standart_has_resource_standart1_idx` (`standart_id_standart` ASC)," +
                "  CONSTRAINT `fk_standart_has_resource_standart1`" +
                "    FOREIGN KEY (`standart_id_standart`)" +
                "    REFERENCES `standart` (`id_standart`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_standart_has_resource_resource1`" +
                "    FOREIGN KEY (`resource_id_resource`)" +
                "    REFERENCES `resource` (`id_resource`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
