package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_16__createTable_collection implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `collection` (" +
                "  `id_collection` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_collection` VARCHAR(250) NOT NULL," +
                "  `code_collection` VARCHAR(50) NOT NULL," +
                "  PRIMARY KEY (`id_collection`))" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
