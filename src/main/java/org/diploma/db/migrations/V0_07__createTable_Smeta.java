package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_07__createTable_Smeta implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `smeta` (" +
                "  `id_smeta` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_smeta` VARCHAR(250) NOT NULL," +
                "  `date_create` DATETIME NOT NULL," +
                "  `average_rank` DECIMAL(10,2) NULL," +
                "  `salary_smeta` DECIMAL(10,2) NULL," +
                "  `complexity_smeta` DECIMAL(10,2) NULL," +
                "  `cost_smeta` DECIMAL(10,2) NULL," +
                "  `projects_id_project` BIGINT NOT NULL," +
                "  `list_works_id_list` BIGINT NOT NULL," +
                "  PRIMARY KEY (`id_smeta`)," +
                "  INDEX `fk_smeta_projects_idx` (`projects_id_project` ASC)," +
                "  INDEX `fk_smeta_list_works1_idx` (`list_works_id_list` ASC)," +
                "  CONSTRAINT `fk_smeta_projects`" +
                "    FOREIGN KEY (`projects_id_project`)" +
                "    REFERENCES `projects` (`id_project`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION," +
                "  CONSTRAINT `fk_smeta_list_works1`" +
                "    FOREIGN KEY (`list_works_id_list`)" +
                "    REFERENCES `list_works` (`id_list`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
