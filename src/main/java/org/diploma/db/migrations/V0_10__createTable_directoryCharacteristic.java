package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_10__createTable_directoryCharacteristic implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `directory_characteristic` (\n" +
                "  `id_directory_characteristic` BIGINT NOT NULL AUTO_INCREMENT,\n" +
                "  `name_directory_characteristic` VARCHAR(250) NOT NULL,\n" +
                "  `units_id_unit` BIGINT NOT NULL,\n" +
                "  PRIMARY KEY (`id_directory_characteristic`),\n" +
                "  INDEX `fk_directory_characteristic_units1_idx` (`units_id_unit` ASC),\n" +
                "  CONSTRAINT `fk_directory_characteristic_units1`\n" +
                "    FOREIGN KEY (`units_id_unit`)\n" +
                "    REFERENCES `units` (`id_unit`)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION)\n" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }


}
