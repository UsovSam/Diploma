package org.diploma.db.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Sam.
 */
public class V0_05__createTable_listWork implements SpringJdbcMigration{
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `list_works` (" +
                "  `id_list` BIGINT NOT NULL AUTO_INCREMENT," +
                "  `name_list` VARCHAR(250) NOT NULL," +
                "  `date_start` DATETIME NULL," +
                "  `date_end` DATETIME NULL," +
                "  `projects_id_project` BIGINT NOT NULL," +
                "  PRIMARY KEY (`id_list`)," +
                "  INDEX `fk_list_works_projects1_idx` (`projects_id_project` ASC)," +
                "  CONSTRAINT `fk_list_works_projects1`" +
                "    FOREIGN KEY (`projects_id_project`)" +
                "    REFERENCES `projects` (`id_project`)" +
                "    ON DELETE NO ACTION" +
                "    ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
    }
}
