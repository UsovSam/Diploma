package org.diploma.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "standart")
public class Standart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_standart")
    private Long id;

    @Column(name = "name_standart")
    private String name;

    @Column(name = "code_standart")
    private String code;

    @ManyToOne
    @JoinColumn(name = "group_id_group", foreignKey = @ForeignKey(name = "fk_standart_group1_idx"))
    private Group group;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "standart_has_resource",
            joinColumns = {@JoinColumn(name = "standart_id_standart", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "resource_id_resource", nullable = false, updatable = false)})
    private List<Resource> resources = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }
}
