package org.diploma.entity;

import javax.persistence.*;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "directory_machine")
public class DirectoryMachine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_directory_machine")
    private Long id;

    @Column(name = "name_directory_machine")
    private String name;

    @ManyToOne
    @JoinColumn(name = "units_id_unit", foreignKey = @ForeignKey(name = "fk_directory_machine_units1_idx"))
    private Units units;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Units getUnits() {
        return units;
    }

    public void setUnits(Units units) {
        this.units = units;
    }
}
