package org.diploma.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "smeta")
public class Smeta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_smeta")
    private Long id;

    @Column(name = "name_smeta")
    private String name;

    @Column(name = "date_create")
    @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    private LocalDateTime dateCreate;

    @Column(name = "average_rank")
    private BigDecimal averageRank;

    @Column(name = "salary_smeta")
    private BigDecimal salary;

    @Column(name = "complexity_smeta")
    private BigDecimal complexity;

    @Column(name = "cost_smeta")
    private BigDecimal cost;

    @ManyToOne
    @JoinColumn(name = "projects_id_project",
            foreignKey = @ForeignKey(name = "fk_smeta_projects_idx"))
    private Project project;

    @ManyToOne
    @JoinColumn(name = "list_works_id_list",
            foreignKey = @ForeignKey(name = "fk_smeta_list_works1_idx"))
    private ListWork listWork;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(LocalDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public BigDecimal getAverageRank() {
        return averageRank;
    }

    public void setAverageRank(BigDecimal averageRank) {
        this.averageRank = averageRank;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public BigDecimal getComplexity() {
        return complexity;
    }

    public void setComplexity(BigDecimal complexity) {
        this.complexity = complexity;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public ListWork getListWork() {
        return listWork;
    }

    public void setListWork(ListWork listWork) {
        this.listWork = listWork;
    }
}
