package org.diploma.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "composition_work")
public class CompositionWork {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_composition_work")
    private Long id;

    @Column(name = "name_composition_work")
    private String name;

    @ManyToOne
    @JoinColumn(name = "group_id_group", foreignKey = @ForeignKey(name = "fk_composition_work_group1_idx"))
    private Group  group;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
