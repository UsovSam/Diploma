package org.diploma.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "section_smeta")
public class SectionSmeta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_section")
    private Long id;

    @Column(name = "name_section")
    private String name;

    @Column(name = "complexity_section")
    private BigDecimal complexity;

    @Column(name = "rank_section")
    private BigDecimal rank;

    @Column(name = "material_cost_section")
    private BigDecimal materialCost;


    @Column(name = "sum_section")
    private BigDecimal totalSum;

    @ManyToOne
    @JoinColumn(name = "smeta_id_smeta", foreignKey = @ForeignKey(name = "fk_section_smeta_smeta1_idx"))
    private Smeta smeta;

}
