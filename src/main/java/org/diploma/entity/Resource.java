package org.diploma.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "resource")
public class Resource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_resource")
    private Long id;

    @Column(name = "value_resource")
    private BigDecimal value;

    @ManyToOne
    @JoinColumn(name = "directory_resource_id_directiory_resource", foreignKey = @ForeignKey(name = "fk_resource_directory_resource1_idx"))
    private DirectoryResource directoryResource;

/*    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "standart_has_resource",
            joinColumns = {@JoinColumn(name = "resource_id_resource", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "standart_id_standart", nullable = false, updatable = false)})
    private List<Standart> standarts = new ArrayList<>();*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public DirectoryResource getDirectoryResource() {
        return directoryResource;
    }

    public void setDirectoryResource(DirectoryResource directoryResource) {
        this.directoryResource = directoryResource;
    }

/*
    public List<Standart> getStandarts() {
        return standarts;
    }

    public void setStandarts(List<Standart> standarts) {
        this.standarts = standarts;
    }
*/

}
