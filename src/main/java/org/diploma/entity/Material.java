package org.diploma.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "material")
public class Material {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_material")
    private Long id;

    @Column(name = "count")
    private BigDecimal count;

    @Column(name = "price")
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name="section_smeta_id_section", foreignKey = @ForeignKey(name = "fk_material_section_smeta1_idx"))
    private SectionSmeta sectionSmeta;

    @ManyToOne
    @JoinColumn(name="directory_resource_id_directiory_resource", foreignKey = @ForeignKey(name = "fk_material_directory_resource1_idx"))
    private DirectoryResource directoryResource;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCount() {
        return count;
    }

    public void setCount(BigDecimal count) {
        this.count = count;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public SectionSmeta getSectionSmeta() {
        return sectionSmeta;
    }

    public void setSectionSmeta(SectionSmeta sectionSmeta) {
        this.sectionSmeta = sectionSmeta;
    }

    public DirectoryResource getDirectoryResource() {
        return directoryResource;
    }

    public void setDirectoryResource(DirectoryResource directoryResource) {
        this.directoryResource = directoryResource;
    }
}
