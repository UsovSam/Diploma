package org.diploma.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "directory_resource")
public class DirectoryResource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_directiory_resource")
    private Long id;

    @Column(name = "type_resource")
    private Integer type;

    @Column(name = "code_resource")
    private String code;

    @Column(name = "price_resource")
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "directory_machine_id_directory_machine", foreignKey = @ForeignKey(name = "fk_directory_resource_directory_machine1_idx"))
    private DirectoryMachine directoryMachine;

    @ManyToOne
    @JoinColumn(name = "directory_material_id_directory_material", foreignKey = @ForeignKey(name = "fk_directory_resource_directory_material1_idx"))
    private DirectoryMaterial directoryMaterial;

    @ManyToOne
    @JoinColumn(name = "directory_characteristic_id_directory_characteristic", foreignKey = @ForeignKey(name = "fk_directory_resource_directory_characteristic1_idx"))
    private DirectoryCharacteristic directoryCharacteristic;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public DirectoryMachine getDirectoryMachine() {
        return directoryMachine;
    }

    public void setDirectoryMachine(DirectoryMachine directoryMachine) {
        this.directoryMachine = directoryMachine;
    }

    public DirectoryMaterial getDirectoryMaterial() {
        return directoryMaterial;
    }

    public void setDirectoryMaterial(DirectoryMaterial directoryMaterial) {
        this.directoryMaterial = directoryMaterial;
    }

    public DirectoryCharacteristic getDirectoryCharacteristic() {
        return directoryCharacteristic;
    }

    public void setDirectoryCharacteristic(DirectoryCharacteristic directoryCharacteristic) {
        this.directoryCharacteristic = directoryCharacteristic;
    }
}
