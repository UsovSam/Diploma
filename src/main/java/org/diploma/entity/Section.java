package org.diploma.entity;

import javax.persistence.*;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "section")
public class Section {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_section")
    private Long id;

    @Column(name = "name_section")
    private String name;

    @Column(name = "code_section")
    private String code;

    @ManyToOne
    @JoinColumn(name = "collection_id_collection", foreignKey = @ForeignKey(name = "fk_section_collection1_idx"))
    private Collection collection;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Collection getCollection() {
        return collection;
    }

    public void setCollection(Collection collection) {
        this.collection = collection;
    }
}
