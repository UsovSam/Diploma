package org.diploma.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "generalized_works")
public class GeneralizedWork {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_generelized")
    private Long id;

    @Column(name = "name_generalized")
    private String name;

    @Column(name = "value_generalized")
    private BigDecimal value;

    @Column(name = "count_day")
    private BigDecimal days;

    @Column(name = "count_workers")
    private BigDecimal workers;

    @Column(name = "prev_work")
    private String prevWork;

    @ManyToOne
    @JoinColumn(name = "list_works_id_list", foreignKey = @ForeignKey(name = "fk_generalized_works_list_works1_idx"))
    private ListWork listWork;

    @ManyToOne
    @JoinColumn(name = "units_id_unit", foreignKey = @ForeignKey(name = "fk_generalized_works_units1_idx"))
    private Units units;

    @ManyToOne
    @JoinColumn(name = "work_id_work", foreignKey = @ForeignKey(name = "fk_generalized_works_work1_idx"))
    private Work work;

    @ManyToOne
    @JoinColumn(name = "machine_id_machine", foreignKey = @ForeignKey(name = "fk_generalized_works_machine1_idx"))
    private Machine machine;

    @ManyToOne
    @JoinColumn(name = "material_id_material", foreignKey = @ForeignKey(name = "fk_generalized_works_material1_idx"))
    private Material material;

    @ManyToOne
    @JoinColumn(name = "standart_id_standart", foreignKey = @ForeignKey(name = "fk_generalized_works_standart1_idx"))
    private Standart standart;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public ListWork getListWork() {
        return listWork;
    }

    public void setListWork(ListWork listWork) {
        this.listWork = listWork;
    }

    public Units getUnits() {
        return units;
    }

    public void setUnits(Units units) {
        this.units = units;
    }

    public Work getWork() {
        return work;
    }

    public void setWork(Work work) {
        this.work = work;
    }

    public Machine getMachine() {
        return machine;
    }

    public void setMachine(Machine machine) {
        this.machine = machine;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Standart getStandart() {
        return standart;
    }

    public void setStandart(Standart standart) {
        this.standart = standart;
    }

    public BigDecimal getDays() {
        return days;
    }

    public void setDays(BigDecimal days) {
        this.days = days;
    }

    public BigDecimal getWorkers() {
        return workers;
    }

    public void setWorkers(BigDecimal workers) {
        this.workers = workers;
    }

    public String getPrevWork() {
        return prevWork;
    }

    public void setPrevWork(String prevWork) {
        this.prevWork = prevWork;
    }
}
