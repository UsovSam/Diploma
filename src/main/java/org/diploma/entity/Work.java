package org.diploma.entity;

import jdk.nashorn.internal.scripts.JO;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "work")
public class Work {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_work")
    private Long id;

    @Column(name = "count")
    private BigDecimal count;

    @Column(name = "price")
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name="section_smeta_id_section", foreignKey = @ForeignKey(name = "fk_work_section_smeta1_idx"))
    private SectionSmeta sectionSmeta;

    @ManyToOne
    @JoinColumn(name="directory_resource_id_directiory_resource", foreignKey = @ForeignKey(name = "fk_work_directory_resource1_idx"))
    private DirectoryResource directoryResource;

}
