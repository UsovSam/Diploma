package org.diploma.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Sam.
 */
@Entity
@Table(name = "`group`")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id_group`")
    private Long id;

    @Column(name = "`name_group`")
    private String name;

    @Column(name = "`code_group`")
    private String code;

    @Column(name = "`measure_group`")
    private BigDecimal measure;

    @ManyToOne
    @JoinColumn(name = "`section_id_section`", foreignKey = @ForeignKey(name = "fk_group_section1_idx"))
    private Section section;

    @ManyToOne
    @JoinColumn(name = "`units_id_unit`", foreignKey = @ForeignKey(name = "fk_group_units1_idx"))
    private Units units;


    public Units getUnits() {
        return units;
    }

    public void setUnits(Units units) {
        this.units = units;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getMeasure() {
        return measure;
    }

    public void setMeasure(BigDecimal measure) {
        this.measure = measure;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }
}
