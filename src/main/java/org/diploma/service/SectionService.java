package org.diploma.service;

import org.diploma.entity.Section;

import java.util.List;

/**
 * Created by Sam.
 */
public interface SectionService {

    public List<Section> getAll();

    public Section findById(Long id);

    public void deleteById(Long id);

    public Section save(Section section);
}
