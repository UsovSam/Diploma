package org.diploma.service.imp;

import org.diploma.entity.Resource;
import org.diploma.repository.ResourceRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.ResourceService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class ResourceServiceImp implements ResourceService {
    @Autowired
    private ResourceRepository resourceRepository;

    @Override
    public List<Resource> getAll() {
        return null;
    }

    @Override
    public Resource findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public Resource save(Resource resource) {
        return null;
    }
}
