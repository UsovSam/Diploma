package org.diploma.service.imp;

import org.diploma.entity.Project;
import org.diploma.entity.User;
import org.diploma.repository.ProjectRepository;
import org.diploma.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class ProjectServiceImp implements ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public List<Project> getAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project findById(Long id) {
        return projectRepository.findOne(id);
    }

    @Override
    public void deleteById(Long id) {
        projectRepository.delete(id);
    }

    @Override
    public void save(Project project) {
        projectRepository.saveAndFlush(project);
    }

    @Override
    public List<Project> findByUser(User user) {
        return projectRepository.findByUsers(user);
    }
}
