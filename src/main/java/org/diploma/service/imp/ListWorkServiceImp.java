package org.diploma.service.imp;

import org.diploma.entity.ListWork;
import org.diploma.entity.Project;
import org.diploma.repository.ListWorkRepository;
import org.diploma.repository.ProjectRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.ListWorkService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class ListWorkServiceImp implements ListWorkService {
    @Autowired
    private ListWorkRepository listWorkRepository;
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public List<ListWork> getAll() {
        return listWorkRepository.findAll();
    }

    @Override
    public ListWork findById(Long id) {
        return listWorkRepository.findOne(id);
    }

    @Override
    public void deleteById(Long id) {
        listWorkRepository.delete(id);
    }

    @Override
    public ListWork save(ListWork listWork) {
        return listWorkRepository.saveAndFlush(listWork);
    }

    @Override
    public List<ListWork> findAllByProject(Long idProject) {
        return listWorkRepository.findByProject(projectRepository.findOne(idProject));
    }

    @Override
    public List<ListWork> findAllByProjects(List<Project> projectList) {
        List<ListWork> listWorks = new ArrayList<>();
        for(Project project : projectList){
            listWorks.addAll(findAllByProject(project.getId()));
        }
        return listWorks;
    }
}
