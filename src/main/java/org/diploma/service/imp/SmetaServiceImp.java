package org.diploma.service.imp;

import org.diploma.entity.Project;
import org.diploma.entity.Smeta;
import org.diploma.entity.User;
import org.diploma.repository.RoleRepository;
import org.diploma.repository.SmetaRepository;
import org.diploma.service.ProjectService;
import org.diploma.service.RoleService;
import org.diploma.service.SmetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class SmetaServiceImp implements SmetaService {

    @Autowired
    private SmetaRepository smetaRepository;

    @Autowired
    private ProjectService projectService;

    @Override
    public List<Smeta> getAll() {
        return smetaRepository.findAll();
    }

    @Override
    public Smeta findById(Long id) {
        return smetaRepository.findOne(id);
    }

    @Override
    public void deleteById(Long id) {
smetaRepository.delete(id);
    }

    @Override
    public Smeta save(Smeta smeta) {
        return smetaRepository.save(smeta);
    }

    @Override
    public List<Smeta> findAllByUser(User user) {
        List<Smeta> list = new ArrayList<>();
        for(Project project : user.getProjects()){
            list.addAll(smetaRepository.findByProject(project));
        }
        return list;
    }
}
