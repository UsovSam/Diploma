package org.diploma.service.imp;

import org.diploma.entity.DirectoryMachine;
import org.diploma.repository.DirectoryMachineRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.DirectoryMachineService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class DirectoryMachineServiceImp implements DirectoryMachineService {
    @Autowired
    private DirectoryMachineRepository directoryMachineRepository;

    @Override
    public List<DirectoryMachine> getAll() {
        return null;
    }

    @Override
    public DirectoryMachine findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public DirectoryMachine save(DirectoryMachine machine) {
        return null;
    }
}
