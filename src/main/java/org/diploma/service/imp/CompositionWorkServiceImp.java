package org.diploma.service.imp;

import org.diploma.entity.CompositionWork;
import org.diploma.repository.CompositionWorkRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.CompositionWorkService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class CompositionWorkServiceImp implements CompositionWorkService {
    @Autowired
    private CompositionWorkRepository compositionWorkRepository;

    @Override
    public List<CompositionWork> getAll() {
        return null;
    }

    @Override
    public CompositionWork findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public CompositionWork save(CompositionWork compositionWork) {
        return null;
    }
}
