package org.diploma.service.imp;

import org.diploma.entity.Units;
import org.diploma.repository.RoleRepository;
import org.diploma.repository.UnitsRepository;
import org.diploma.service.RoleService;
import org.diploma.service.UnitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class UnitsServiceImp implements UnitsService {
    @Autowired
    private UnitsRepository unitsRepository;

    @Override
    public List<Units> getAll() {
        return unitsRepository.findAll();
    }

    @Override
    public Units findById(Long id) {
        return unitsRepository.findOne(id);
    }

    @Override
    public void deleteById(Long id) {
        unitsRepository.delete(id);
    }

    @Override
    public Units save(Units units) {
        return unitsRepository.saveAndFlush(units);
    }
}
