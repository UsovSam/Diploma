package org.diploma.service.imp;

import org.diploma.entity.DirectoryMaterial;
import org.diploma.repository.DirectoryMaterialRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.DirectoryMaterialService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class DirectoryMaterialServiceImp implements DirectoryMaterialService {
    @Autowired
    private DirectoryMaterialRepository directoryMaterialRepository;

    @Override
    public List<DirectoryMaterial> getAll() {
        return null;
    }

    @Override
    public DirectoryMaterial findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public DirectoryMaterial save(DirectoryMaterial material) {
        return null;
    }
}
