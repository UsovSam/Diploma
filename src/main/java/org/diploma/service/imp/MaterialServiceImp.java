package org.diploma.service.imp;

import org.diploma.entity.Material;
import org.diploma.repository.MaterialRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.MaterialService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class MaterialServiceImp implements MaterialService {
    @Autowired
    private MaterialRepository materialRepository;

    @Override
    public List<Material> getAll() {
        return null;
    }

    @Override
    public Material findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public Material save(Material material) {
        return null;
    }
}
