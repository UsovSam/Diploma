package org.diploma.service.imp;

import org.diploma.entity.DirectoryCharacteristic;
import org.diploma.repository.DirectoryCharacteristicRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.DirectoryCharacteristicService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class DirectoryCharacteristicServiceImp implements DirectoryCharacteristicService {
    @Autowired
    private DirectoryCharacteristicRepository directoryCharacteristicRepository;

    @Override
    public List<DirectoryCharacteristic> getAll() {
        return null;
    }

    @Override
    public DirectoryCharacteristic findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public DirectoryCharacteristic save(DirectoryCharacteristic characteristic) {
        return null;
    }
}
