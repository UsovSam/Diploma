package org.diploma.service.imp;

import org.diploma.entity.GeneralizedWork;
import org.diploma.entity.ListWork;
import org.diploma.repository.GeneralizedWorkRepository;
import org.diploma.service.GeneralizedWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class GeneralizedWorkServiceImp implements GeneralizedWorkService {
    @Autowired
    private GeneralizedWorkRepository generalizedWorkRepository;

    @Override
    public List<GeneralizedWork> getAll() {
        return generalizedWorkRepository.findAll();
    }

    @Override
    public GeneralizedWork findById(Long id) {
        return generalizedWorkRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        generalizedWorkRepository.delete(id);
    }

    @Override
    public GeneralizedWork save(GeneralizedWork generalizedWork) {
        return generalizedWorkRepository.saveAndFlush(generalizedWork);
    }

    @Override
    public List<GeneralizedWork> findAllByListWork(ListWork idListWork) {
        return generalizedWorkRepository.findByListWork(idListWork);
    }
}
