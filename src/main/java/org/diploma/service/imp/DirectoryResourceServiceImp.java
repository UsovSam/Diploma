package org.diploma.service.imp;

import org.diploma.entity.DirectoryResource;
import org.diploma.repository.DirectoryResourceRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.DirectoryResourceService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class DirectoryResourceServiceImp implements DirectoryResourceService {
    @Autowired
    private DirectoryResourceRepository directoryResourceRepository;

    @Override
    public List<DirectoryResource> getAll() {
        return null;
    }

    @Override
    public DirectoryResource findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public DirectoryResource save(DirectoryResource resource) {
        return null;
    }
}
