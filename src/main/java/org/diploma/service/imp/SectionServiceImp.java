package org.diploma.service.imp;

import org.diploma.entity.Section;
import org.diploma.repository.RoleRepository;
import org.diploma.repository.SectionRepository;
import org.diploma.service.RoleService;
import org.diploma.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class SectionServiceImp implements SectionService {
    @Autowired
    private SectionRepository sectionRepository;

    @Override
    public List<Section> getAll() {
        return sectionRepository.findAll();
    }

    @Override
    public Section findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public Section save(Section section) {
        return null;
    }
}
