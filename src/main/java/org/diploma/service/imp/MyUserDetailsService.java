package org.diploma.service.imp;

import org.diploma.entity.Role;
import org.diploma.entity.User;
import org.diploma.security.MyUserDetails;
import org.diploma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by Sam.
 */
@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    @Transactional(transactionManager = "transactionManager", readOnly = true)
    public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
        User user = userService.getByLogin(string);
        if (user == null) throw new UsernameNotFoundException("User not found");
        return new MyUserDetails(user, getAuthority(user.getRole()));
    }

    private Collection<GrantedAuthority> getAuthority(Role role) {
        Collection<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(role.getName()));
        return authorities;
    }
}
