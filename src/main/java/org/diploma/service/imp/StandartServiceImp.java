package org.diploma.service.imp;

import org.diploma.entity.Standart;
import org.diploma.repository.RoleRepository;
import org.diploma.repository.StandartRepository;
import org.diploma.repository.specification.StandartSpecification;
import org.diploma.service.RoleService;
import org.diploma.service.StandartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class StandartServiceImp implements StandartService {
    @Autowired
    private StandartRepository standartRepository;

    @Override
    public List<Standart> getAll() {
        return standartRepository.findAll();
    }

    @Override
    public Standart findById(Long id) {
        return standartRepository.findOne(id);
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public Standart save(Standart standart) {
        return null;
    }

    @Override
    public List<Standart> findByWorkName(String work) {
        return standartRepository.findByNameContainingIgnoreCase(work);
    }

    @Override
    public List<Standart> findByParameter(Long collect, Long group, String work) {
        return standartRepository.findAll(StandartSpecification.getOrdersByFilter(collect, group, work));
    }
}
