package org.diploma.service.imp;

import org.diploma.entity.SectionSmeta;
import org.diploma.repository.RoleRepository;
import org.diploma.repository.SectionSmetaRepository;
import org.diploma.service.RoleService;
import org.diploma.service.SectionSmetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class SectionSmetaServiceImp implements SectionSmetaService {
    @Autowired
    private SectionSmetaRepository sectionSmetaRepository;

    @Override
    public List<SectionSmeta> getAll() {
        return null;
    }

    @Override
    public SectionSmeta findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public SectionSmeta save(SectionSmeta sectionSmeta) {
        return null;
    }
}
