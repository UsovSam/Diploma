package org.diploma.service.imp;

import org.diploma.entity.User;
import org.diploma.repository.RoleRepository;
import org.diploma.repository.UserRepository;
import org.diploma.service.RoleService;
import org.diploma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Sam.
 */
@Service
public class UserServiceImp implements UserService {

    private static final long idRoleUser = 2L;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleService roleService;
    @Autowired
    private BCryptPasswordEncoder bcrypt;

    @Override
    public User addUser(User user) {
        user.setPassword(encodePassword(user.getPassword().trim()));
        user.setRole(roleService.findById(idRoleUser));
        return userRepository.save(user);
    }

    @Override
    public User getById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User save(final User user) {
        return userRepository.save(user);

    }

    @Override
    public void deleteById(Long id) {
        User user = userRepository.getOne(id);
        userRepository.save(user);
    }

    @Override
    public User getByLogin(String name) {
        return userRepository.findFirstByLogin(name);
    }

    @Override
    public String encodePassword(String password) {
        return bcrypt.encode(password);
    }

    @Override
    public Boolean mathcPassword(String rawPassword, String encodedPassword) {
        return bcrypt.matches(rawPassword, encodedPassword);
    }


}
