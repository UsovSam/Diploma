package org.diploma.service.imp;

import org.diploma.entity.Work;
import org.diploma.repository.RoleRepository;
import org.diploma.repository.WorkRepository;
import org.diploma.service.RoleService;
import org.diploma.service.WorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class WorkServiceImp implements WorkService {
    @Autowired
    private WorkRepository workRepository;

    @Override
    public List<Work> getAll() {
        return null;
    }

    @Override
    public Work findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public Work save(Work work) {
        return null;
    }
}
