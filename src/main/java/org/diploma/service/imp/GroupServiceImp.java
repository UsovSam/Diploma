package org.diploma.service.imp;

import org.diploma.entity.Group;
import org.diploma.repository.GroupRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.GroupService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * Created by Sam.
 */
@Service
public class GroupServiceImp implements GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Override
    public List<Group> getAll() {
        return groupRepository.findAll();
    }

    @Override
    public Group findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public Group save(Group group) {
        return null;
    }

    @Override
    public List<Group> findByIdProject(Long idProject) {
        return groupRepository.findAll().stream().filter(e -> Objects.equals(e.getSection().getCollection().getId(), idProject)).collect(Collectors.toList());
    }
}
