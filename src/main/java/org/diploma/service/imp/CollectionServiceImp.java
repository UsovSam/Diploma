package org.diploma.service.imp;

import org.diploma.entity.Collection;
import org.diploma.repository.CollectionRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.CollectionService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class CollectionServiceImp implements CollectionService {
    @Autowired
    private CollectionRepository collectionRepository;

    @Override
    public List<Collection> getAll() {
        return collectionRepository.findAll();
    }

    @Override
    public Collection findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public Collection save(Collection collection) {
        return null;
    }
}
