package org.diploma.service.imp;

import org.diploma.entity.Machine;
import org.diploma.repository.MachineRepository;
import org.diploma.repository.RoleRepository;
import org.diploma.service.MachineService;
import org.diploma.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Sam.
 */
@Service
public class MachineServiceImp implements MachineService {
    @Autowired
    private MachineRepository machineRepository;

    @Override
    public List<Machine> getAll() {
        return null;
    }

    @Override
    public Machine findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public Machine save(Machine machine) {
        return null;
    }
}
