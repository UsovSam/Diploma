package org.diploma.service;

import org.diploma.entity.DirectoryResource;

import java.util.List;

/**
 * Created by Sam.
 */
public interface DirectoryResourceService {

    public List<DirectoryResource> getAll();

    public DirectoryResource findById(Long id);

    public void deleteById(Long id);

    public DirectoryResource save(DirectoryResource resource);
}
