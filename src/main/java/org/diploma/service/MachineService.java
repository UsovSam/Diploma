package org.diploma.service;

import org.diploma.entity.Machine;

import java.util.List;

/**
 * Created by Sam.
 */
public interface MachineService {

    public List<Machine> getAll();

    public Machine findById(Long id);

    public void deleteById(Long id);

    public Machine save(Machine machine);
}
