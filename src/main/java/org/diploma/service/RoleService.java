package org.diploma.service;


import org.diploma.entity.Role;

import java.util.List;

/**
 * Created by Sam.
 */
public interface RoleService {

    public List<Role> getRoles();

    Role findById(Long id);

}
