package org.diploma.service;

import org.diploma.entity.Material;

import java.util.List;

/**
 * Created by Sam.
 */
public interface MaterialService {

    public List<Material> getAll();

    public Material findById(Long id);

    public void deleteById(Long id);

    public Material save(Material material);
}
