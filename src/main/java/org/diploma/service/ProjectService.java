package org.diploma.service;

import org.diploma.entity.Project;
import org.diploma.entity.User;

import java.util.List;

/**
 * Created by Sam.
 */
public interface ProjectService {

    public List<Project> getAll();

    public Project findById(Long id);

    public void deleteById(Long id);

    public void save(Project project);

    List<Project> findByUser(User user);
}
