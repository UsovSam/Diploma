package org.diploma.service;

import org.diploma.entity.DirectoryMachine;

import java.util.List;

/**
 * Created by Sam.
 */
public interface DirectoryMachineService {

    public List<DirectoryMachine> getAll();

    public DirectoryMachine findById(Long id);

    public void deleteById(Long id);

    public DirectoryMachine save(DirectoryMachine machine);
}
