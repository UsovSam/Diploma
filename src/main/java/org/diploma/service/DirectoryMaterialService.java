package org.diploma.service;

import org.diploma.entity.DirectoryMaterial;

import java.util.List;

/**
 * Created by Sam.
 */
public interface DirectoryMaterialService {

    public List<DirectoryMaterial> getAll();

    public DirectoryMaterial findById(Long id);

    public void deleteById(Long id);

    public DirectoryMaterial save(DirectoryMaterial material);
}
