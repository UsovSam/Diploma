package org.diploma.service;

import org.diploma.entity.CompositionWork;

import java.util.List;

/**
 * Created by Sam.
 */
public interface CompositionWorkService {

    public List<CompositionWork> getAll();

    public CompositionWork findById(Long id);

    public void deleteById(Long id);

    public CompositionWork save(CompositionWork work);
}
