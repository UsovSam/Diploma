package org.diploma.service;

import org.diploma.entity.SectionSmeta;

import java.util.List;

/**
 * Created by Sam.
 */
public interface SectionSmetaService {

    public List<SectionSmeta> getAll();

    public SectionSmeta findById(Long id);

    public void deleteById(Long id);

    public SectionSmeta save(SectionSmeta sectionSmeta);
}
