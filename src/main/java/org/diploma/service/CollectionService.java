package org.diploma.service;

import org.diploma.entity.Collection;

import java.util.List;

/**
 * Created by Sam.
 */
public interface CollectionService {

    public List<Collection> getAll();

    public Collection findById(Long id);

    public void deleteById(Long id);

    public Collection save(Collection collection);
}
