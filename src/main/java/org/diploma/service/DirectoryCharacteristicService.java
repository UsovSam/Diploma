package org.diploma.service;

import org.diploma.entity.DirectoryCharacteristic;

import java.util.List;

/**
 * Created by Sam.
 */
public interface DirectoryCharacteristicService {

    public List<DirectoryCharacteristic> getAll();

    public DirectoryCharacteristic findById(Long id);

    public void deleteById(Long id);

    public DirectoryCharacteristic save(DirectoryCharacteristic directoryCharacteristic);
}
