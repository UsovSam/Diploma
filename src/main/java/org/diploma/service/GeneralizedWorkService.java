package org.diploma.service;

import org.diploma.entity.GeneralizedWork;
import org.diploma.entity.ListWork;

import java.util.List;

/**
 * Created by Sam.
 */
public interface GeneralizedWorkService {

    public List<GeneralizedWork> getAll();

    public GeneralizedWork findById(Long id);

    public void deleteById(Long id);

    public GeneralizedWork save(GeneralizedWork generalizedWork);

    List<GeneralizedWork> findAllByListWork(ListWork idListWork);
}
