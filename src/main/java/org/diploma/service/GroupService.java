package org.diploma.service;

import org.diploma.entity.Group;

import java.util.List;

/**
 * Created by Sam.
 */
public interface GroupService {

    public List<Group> getAll();

    public Group findById(Long id);

    public void deleteById(Long id);

    public Group save(Group group);

    List<Group> findByIdProject(Long idProject);
}
