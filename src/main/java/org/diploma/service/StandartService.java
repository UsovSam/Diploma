package org.diploma.service;

import org.diploma.entity.Standart;

import java.util.List;

/**
 * Created by Sam.
 */
public interface StandartService {

    public List<Standart> getAll();

    public Standart findById(Long id);

    public void deleteById(Long id);

    public Standart save(Standart standart);

    List<Standart> findByWorkName(String work);

    List<Standart> findByParameter(Long collect, Long group, String work);
}
