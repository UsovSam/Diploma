package org.diploma.service;

import org.diploma.entity.ListWork;
import org.diploma.entity.Project;

import java.util.List;

/**
 * Created by Sam.
 */
public interface ListWorkService {

    public List<ListWork> getAll();

    public ListWork findById(Long id);

    public void deleteById(Long id);

    public ListWork save(ListWork listWork);

    List<ListWork> findAllByProject(Long idProject);

    List<ListWork> findAllByProjects(List<Project> projectList);
}
