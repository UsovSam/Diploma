package org.diploma.service;

import org.diploma.entity.Units;

import java.util.List;

/**
 * Created by Sam.
 */
public interface UnitsService {

    public List<Units> getAll();

    public Units findById(Long id);

    public void deleteById(Long id);

    public Units save(Units units);
}
