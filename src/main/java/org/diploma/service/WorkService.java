package org.diploma.service;

import org.diploma.entity.Work;

import java.util.List;

/**
 * Created by Sam.
 */
public interface WorkService {

    public List<Work> getAll();

    public Work findById(Long id);

    public void deleteById(Long id);

    public Work save(Work work);
}
