package org.diploma.service;

import org.diploma.entity.Smeta;
import org.diploma.entity.User;

import java.util.List;

/**
 * Created by Sam.
 */
public interface SmetaService {

    public List<Smeta> getAll();

    public Smeta findById(Long id);

    public void deleteById(Long id);

    public Smeta save(Smeta smeta);

    List<Smeta> findAllByUser(User user);
}
