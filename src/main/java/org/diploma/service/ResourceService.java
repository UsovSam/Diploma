package org.diploma.service;

import org.diploma.entity.Resource;

import java.util.List;

/**
 * Created by Sam.
 */
public interface ResourceService {

    public List<Resource> getAll();

    public Resource findById(Long id);

    public void deleteById(Long id);

    public Resource save(Resource resource);
}
