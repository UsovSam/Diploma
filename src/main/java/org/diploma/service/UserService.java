package org.diploma.service;


import org.diploma.entity.User;

import java.util.List;

/**
 * Created by Sam.
 */
public interface UserService {

    public User addUser(User user);

    public User getById(Long id);

    public User save(User object);

    public User getByLogin(String login);

    public void deleteById(Long id);

    public String encodePassword(String password);

    public Boolean mathcPassword(String rawPassword, String encodedPassword);

}
