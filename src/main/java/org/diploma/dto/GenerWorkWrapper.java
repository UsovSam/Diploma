package org.diploma.dto;

import org.diploma.entity.*;

import java.math.BigDecimal;

/**
 * Created by Sam.
 */
public class GenerWorkWrapper {

    private Long id;
    private String name;
    private BigDecimal value;
    private String units;
    private String normative;

    public GenerWorkWrapper() {
    }

    public GenerWorkWrapper(GeneralizedWork work) {
        this.id = work.getId();
        this.name = work.getName();
        this.value = work.getValue();
        this.units = work.getUnits().getName();
        this.normative = work.getStandart() != null ? work.getStandart().getCode() : "";
    }

    public String getNormative() {
        return normative;
    }

    public void setNormative(String normative) {
        this.normative = normative;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }
}
