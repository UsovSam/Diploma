package org.diploma.repository;

import org.diploma.entity.GeneralizedWork;
import org.diploma.entity.ListWork;
import org.diploma.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Sam.
 */
public interface GeneralizedWorkRepository extends JpaRepository<GeneralizedWork, Long> {
    List<GeneralizedWork> findByListWork(ListWork listWork);
    GeneralizedWork findById(Long id);
}
