package org.diploma.repository;

import org.diploma.entity.CompositionWork;
import org.diploma.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface CompositionWorkRepository extends JpaRepository<CompositionWork, Long> {
}
