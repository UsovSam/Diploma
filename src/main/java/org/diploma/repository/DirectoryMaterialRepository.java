package org.diploma.repository;

import org.diploma.entity.DirectoryMaterial;
import org.diploma.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface DirectoryMaterialRepository extends JpaRepository<DirectoryMaterial, Long> {
}
