package org.diploma.repository;

import org.diploma.entity.Project;
import org.diploma.entity.SectionSmeta;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface SectionSmetaRepository extends JpaRepository<SectionSmeta, Long> {
}
