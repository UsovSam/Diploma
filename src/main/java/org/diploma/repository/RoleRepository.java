package org.diploma.repository;


import org.diploma.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
}
