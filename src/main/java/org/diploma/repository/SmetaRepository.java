package org.diploma.repository;

import org.diploma.entity.Project;
import org.diploma.entity.Smeta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

/**
 * Created by Sam.
 */
public interface SmetaRepository extends JpaRepository<Smeta, Long> {

    List<Smeta> findByProject(Project project);
    
}
