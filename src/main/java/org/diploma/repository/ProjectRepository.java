package org.diploma.repository;

import org.diploma.entity.Project;
import org.diploma.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Sam.
 */
public interface ProjectRepository  extends JpaRepository<Project, Long> {
    List<Project> findByUsers(User user);
}
