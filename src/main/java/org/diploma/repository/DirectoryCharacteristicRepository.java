package org.diploma.repository;

import org.diploma.entity.DirectoryCharacteristic;
import org.diploma.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface DirectoryCharacteristicRepository extends JpaRepository<DirectoryCharacteristic, Long> {
}
