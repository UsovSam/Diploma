package org.diploma.repository;

import org.diploma.entity.Project;
import org.diploma.entity.Units;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface UnitsRepository extends JpaRepository<Units, Long> {
}
