package org.diploma.repository;

import org.diploma.entity.DirectoryMachine;
import org.diploma.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface DirectoryMachineRepository extends JpaRepository<DirectoryMachine, Long> {
}
