package org.diploma.repository;

import org.diploma.entity.Machine;
import org.diploma.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface MachineRepository extends JpaRepository<Machine, Long> {
}
