package org.diploma.repository.specification;

import org.diploma.entity.Standart;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sam
 */
public class StandartSpecification {

    public static Specification<Standart> getOrdersByFilter(final Long collect, final Long group, String work) {
        return (root, cq, cb) -> {

            final List predicates = new ArrayList();

            if (collect != null)
                predicates.add(cb.equal(root.get("group").get("section").get("collection").<Long>get("id"), collect));

            if (group != null) {
                predicates.add(cb.equal(root.get("group").<Long>get("id"), group));
            }
            if (work != null && !work.isEmpty()) {
                predicates.add(cb.like(cb.lower(root.get("group").get("name")), "%" + work.toLowerCase() + "%"));
            }

            Predicate[] predicateArray = (Predicate[]) predicates.toArray(new Predicate[predicates.size()]);
            return cb.and(predicateArray);
        };
    }
}
