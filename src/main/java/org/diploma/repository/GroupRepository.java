package org.diploma.repository;

import org.diploma.entity.Group;
import org.diploma.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Sam.
 */
public interface GroupRepository extends JpaRepository<Group, Long> {

}
