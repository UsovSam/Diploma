package org.diploma.repository;


import org.diploma.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Sam.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    public User findFirstByLogin(String login);

}
