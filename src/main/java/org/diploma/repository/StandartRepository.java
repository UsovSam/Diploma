package org.diploma.repository;

import org.diploma.entity.Project;
import org.diploma.entity.Standart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Sam.
 */
public interface StandartRepository extends JpaRepository<Standart, Long>, JpaSpecificationExecutor<Standart> {

    List<Standart> findByNameContainingIgnoreCase(String name);
}
