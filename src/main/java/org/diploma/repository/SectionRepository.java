package org.diploma.repository;

import org.diploma.entity.Project;
import org.diploma.entity.Section;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface SectionRepository extends JpaRepository<Section, Long> {
}
