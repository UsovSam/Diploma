package org.diploma.repository;

import org.diploma.entity.Material;
import org.diploma.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface MaterialRepository extends JpaRepository<Material, Long> {
}
