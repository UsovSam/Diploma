package org.diploma.repository;

import org.diploma.entity.Project;
import org.diploma.entity.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface ResourceRepository extends JpaRepository<Resource, Long> {
}
