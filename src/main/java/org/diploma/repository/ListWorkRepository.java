package org.diploma.repository;

import org.diploma.entity.ListWork;
import org.diploma.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Created by Sam.
 */
public interface ListWorkRepository extends JpaRepository<ListWork, Long>, JpaSpecificationExecutor<ListWork> {
    List<ListWork> findByProject(Project project);
}
