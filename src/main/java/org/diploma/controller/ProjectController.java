package org.diploma.controller;

import org.diploma.entity.Project;
import org.diploma.entity.User;
import org.diploma.security.MyUserDetails;
import org.diploma.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Sam.
 */
@Controller
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping("")
    public ModelAndView getAllProject() {
        ModelAndView modelAndView = new ModelAndView("projects/projects");
        modelAndView.addObject("projects", projectService.getAll());
        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addPageProject() {
        ModelAndView modelAndView = new ModelAndView("projects/project");
        modelAndView.addObject("project", new Project());
        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addProject(Project project, Authentication authentication) {
        MyUserDetails myUserDetails = (MyUserDetails) authentication.getPrincipal();
        List<User> userList = project.getUsers();
        userList.add(myUserDetails.getUser());
        project.setUsers(userList);
        projectService.save(project);
        return "redirect:/projects";
    }


    @RequestMapping("/{id}")
    public ModelAndView getAllProject(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("projects/project");
        modelAndView.addObject("project", projectService.findById(id));
        return modelAndView;
    }

}
