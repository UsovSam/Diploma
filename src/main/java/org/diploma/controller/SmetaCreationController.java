package org.diploma.controller;

import org.diploma.entity.Collection;
import org.diploma.entity.GeneralizedWork;
import org.diploma.entity.Group;
import org.diploma.entity.Standart;
import org.diploma.service.CollectionService;
import org.diploma.service.GeneralizedWorkService;
import org.diploma.service.GroupService;
import org.diploma.service.StandartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Sam.
 */
@Controller
public class SmetaCreationController {

    @Autowired
    private CollectionService collectionService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private StandartService standartService;
    @Autowired
    private GeneralizedWorkService generalizedWorkService;

    @RequestMapping("/collections/json")
    public @ResponseBody
    List<Collection> getAllCollection() {
        return collectionService.getAll();
    }

    @RequestMapping("/group/json/{idProject}")
    public @ResponseBody
    List<Group> getAllGroup(@PathVariable Long idProject) {
        return groupService.findByIdProject(idProject);
    }

    @RequestMapping(value = "/findstandart/json", method = RequestMethod.POST)
    public @ResponseBody
    List<Standart> getAllStandart(Long collect, Long group, String work) {
        List<Standart> list = standartService.findByParameter(collect, group, work);
        list.forEach(e -> e.setCode(e.getGroup().getSection().getCollection().getCode() + "-" + e.getGroup().getCode() + "-" + e.getCode()));
        return list;
    }

    @RequestMapping(value = "/setstandart/json", method = RequestMethod.POST)
    public @ResponseBody
    String setStandart(Long idWork, Long idStandart) {
        Standart standart = standartService.findById(idStandart);
        GeneralizedWork generalizedWork = generalizedWorkService.findById(idWork);
        generalizedWork.setStandart(standart);
        generalizedWorkService.save(generalizedWork);
        return "OK";
    }

    @RequestMapping(value = "/findstandart/{id}/json", method = RequestMethod.GET)
    public @ResponseBody
    Standart getStandart(@PathVariable Long id) {
        Standart standart = standartService.findById(id);
        return standart;
    }

}
