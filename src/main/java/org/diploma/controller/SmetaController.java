package org.diploma.controller;

import org.diploma.entity.Project;
import org.diploma.entity.Smeta;
import org.diploma.entity.User;
import org.diploma.security.MyUserDetails;
import org.diploma.service.ListWorkService;
import org.diploma.service.ProjectService;
import org.diploma.service.SmetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Sam.
 */
@Controller
@RequestMapping("/smeta")
public class SmetaController {

    @Autowired
    private SmetaService smetaService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ListWorkService listWorkService;


    @RequestMapping("")
    public ModelAndView getAllSmeta(Authentication authentication) {
        MyUserDetails myUserDetails = (MyUserDetails) authentication.getPrincipal();
        ModelAndView modelAndView = new ModelAndView("smeta/smetalist");
        modelAndView.addObject("smeta", smetaService.findAllByUser(myUserDetails.getUser()));
        return modelAndView;
    }

    @RequestMapping("/add")
    public ModelAndView addPageSmeta(Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("smeta/smetaadd");
        modelAndView.addObject("smeta", new Smeta());
        modelAndView.addObject("projects", projectService.findByUser(((MyUserDetails) authentication.getPrincipal()).getUser()));
        return modelAndView;
    }

    @RequestMapping("/showpage")
    public ModelAndView showapageSmeta(Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("smeta/showpage");
      return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addSmeta(Smeta smeta, Authentication authentication) {
        smeta.setDateCreate(LocalDateTime.now());
        smetaService.save(smeta);
        return "redirect:/smeta";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView addSmeta(@PathVariable Long id, Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("smeta/smetaadd");
        modelAndView.addObject("smeta", smetaService.findById(id));
        List<Project> projectList = projectService.findByUser(((MyUserDetails) authentication.getPrincipal()).getUser());
        modelAndView.addObject("projects", projectList);
        modelAndView.addObject("listwork", listWorkService.findAllByProjects(projectList));
    return modelAndView;
    }
    @RequestMapping(value = "/{id}/create", method = RequestMethod.GET)
    public ModelAndView createSmeta(@PathVariable Long id, Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("smeta/smetacreate");
        modelAndView.addObject("smeta",smetaService.findById(id));
        return modelAndView;
    }


}
