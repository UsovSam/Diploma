package org.diploma.controller;

import org.diploma.entity.User;
import org.diploma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Sam.
 */
@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/registration")
    public ModelAndView registration(Boolean pass, Boolean exist) {
        ModelAndView modelAndView = new ModelAndView("registration");
        if (pass)
            modelAndView.addObject("pass", true);
        if (exist)
            modelAndView.addObject("exist", true);
        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registrationPost(String username, String password, String repeatPassword, String name, String surname) {
        User user = userService.getByLogin(username);
        if (user == null) {
            if (password.equals(repeatPassword)) {
                user = new User();
                user.setLogin(username);
                user.setPassword(password);
                user.setName(name);
                user.setSurname(surname);
                userService.addUser(user);
                return "redirect:/login";
            }
            return "redirect:/registration?pass=true&exist=false";

        }
        return "redirect:/registration?exist=true&pass=false";
    }

    @RequestMapping({"/", "/index"})
    public String main() {
        return "index";
    }
}
