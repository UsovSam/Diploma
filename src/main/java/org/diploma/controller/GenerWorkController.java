package org.diploma.controller;

import org.diploma.dto.GenerWorkWrapper;
import org.diploma.entity.GeneralizedWork;
import org.diploma.entity.ListWork;
import org.diploma.entity.Project;
import org.diploma.repository.GeneralizedWorkRepository;
import org.diploma.service.GeneralizedWorkService;
import org.diploma.service.ListWorkService;
import org.diploma.service.ProjectService;
import org.diploma.service.UnitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Sam.
 */
@Controller
@RequestMapping("/generwork")
public class GenerWorkController {

    @Autowired
    private GeneralizedWorkService generalizedWorkService;
    @Autowired
    private ListWorkService listWorkService;
    @Autowired
    private UnitsService unitsService;


    @RequestMapping("/{idListWork}")
    public ModelAndView getAllListWork(@PathVariable Long idListWork) {
        ModelAndView modelAndView = new ModelAndView("projects/generwork");
        ListWork listWork = listWorkService.findById(idListWork);
        modelAndView.addObject("listWork", listWork);
        modelAndView.addObject("generwork", generalizedWorkService.findAllByListWork(listWork));
        return modelAndView;
    }

    @RequestMapping("/json/{idListWork}")
    public @ResponseBody
    List<GenerWorkWrapper> geListWork(@PathVariable Long idListWork) {
        ListWork listWork = listWorkService.findById(idListWork);
        return generalizedWorkService.findAllByListWork(listWork).stream().map(GenerWorkWrapper::new).collect(Collectors.toList());
    }

    @RequestMapping(value = "/{idListWork}/add", method = RequestMethod.GET)
    public ModelAndView addPageListWork(@PathVariable Long idListWork) {
        ModelAndView modelAndView = new ModelAndView("projects/generworkadd");
        GeneralizedWork generalizedWork = new GeneralizedWork();
        ListWork listWork = listWorkService.findById(idListWork);
        generalizedWork.setListWork(listWork);
        modelAndView.addObject("generwork", generalizedWork);
        modelAndView.addObject("units", unitsService.getAll());
        modelAndView.addObject("works", generalizedWorkService.findAllByListWork(listWork));
        return modelAndView;
    }

    @RequestMapping(value = "/{idListWork}/add", method = RequestMethod.POST)
    public String addProject(@PathVariable Integer idListWork, GeneralizedWork generalizedWork) {
        generalizedWork.setValue(BigDecimal.valueOf(100));
        generalizedWork.setUnits(unitsService.getAll().get(0));
        generalizedWorkService.save(generalizedWork);
        return "redirect:/generwork/" + idListWork;
    }

    @RequestMapping(value = "/{idListWork}/delete/{idGener}", method = RequestMethod.GET)
    public String addProject(@PathVariable Integer idListWork, @PathVariable Long idGener) {
        System.out.println(idGener);
        generalizedWorkService.deleteById(idGener);
        return "redirect:/generwork/" + idListWork;
    }

    @RequestMapping(value = "/{idListWork}/change/{idGener}", method = RequestMethod.GET)
    public ModelAndView changeListWork(@PathVariable Long idListWork, @PathVariable Long idGener) {
        ModelAndView modelAndView = new ModelAndView("projects/generworkadd");
        ListWork listWork = listWorkService.findById(idListWork);
        GeneralizedWork generalizedWork = generalizedWorkService.findById(idGener);
        //generalizedWork.setListWork(listWorkService.findById(idListWork));
        modelAndView.addObject("generwork", generalizedWork);
        modelAndView.addObject("units", unitsService.getAll());
        modelAndView.addObject("works", generalizedWorkService.findAllByListWork(listWork));
        return modelAndView;
    }

}
