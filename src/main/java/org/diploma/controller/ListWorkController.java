package org.diploma.controller;

import org.diploma.dto.ListWorkWrapper;
import org.diploma.entity.ListWork;
import org.diploma.entity.Project;
import org.diploma.security.MyUserDetails;
import org.diploma.service.ListWorkService;
import org.diploma.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Sam.
 */

@Controller
@RequestMapping("/listwork")
public class ListWorkController {

    @Autowired
    private ListWorkService listWorkService;
    @Autowired
    private ProjectService projectService;

    @RequestMapping("")
    public ModelAndView getAllListWork(Authentication authentication) {
        MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();
        ModelAndView modelAndView = new ModelAndView("projects/listwork");
        List<Project> projectList = userDetails.getUser().getProjects();
        modelAndView.addObject("listworks", listWorkService.findAllByProjects(projectList));
        return modelAndView;
    }


    @RequestMapping("/{idProject}")
    public ModelAndView getAllListWork(@PathVariable Long idProject) {
        ModelAndView modelAndView = new ModelAndView("projects/listwork");
        Project project = projectService.findById(idProject);
        modelAndView.addObject("project", project);
        modelAndView.addObject("listworks", listWorkService.findAllByProject(idProject));
        return modelAndView;
    }


    @RequestMapping("/json/{idProject}")
    public @ResponseBody
    List<ListWorkWrapper> getListWork(@PathVariable Long idProject) {
        Project project = projectService.findById(idProject);
        return listWorkService.findAllByProject(idProject).stream().map(e -> new ListWorkWrapper(e.getId(), e.getName())).collect(Collectors.toList());
    }


    @RequestMapping(value = "/{idProject}/add", method = RequestMethod.GET)
    public ModelAndView addPageListWork(@PathVariable Long idProject) {
        ModelAndView modelAndView = new ModelAndView("projects/listworkadd");
        ListWork listWork = new ListWork();
        listWork.setProject(projectService.findById(idProject));
        modelAndView.addObject("listWork", listWork);
        return modelAndView;
    }

    @RequestMapping(value = "/{idProject}/add", method = RequestMethod.POST)
    public String addProject(@PathVariable Integer idProject, ListWork listWork) {
        listWorkService.save(listWork);
        return "redirect:/listwork/" + idProject;
    }

    @RequestMapping(value = "/{idProject}/change/{idList}", method = RequestMethod.GET)
    public ModelAndView changeListWork(@PathVariable Long idProject, @PathVariable Long idList) {
        ModelAndView modelAndView = new ModelAndView("projects/listworkadd");
        ListWork listWork = listWorkService.findById(idList);
      //  System.out.println(listWork.getProject().getId());
        //listWork.setProject(projectService.findById(idProject));
        modelAndView.addObject("listWork", listWork);
        return modelAndView;
    }


}
