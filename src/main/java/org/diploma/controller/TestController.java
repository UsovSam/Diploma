package org.diploma.controller;

import org.diploma.entity.GeneralizedWork;
import org.diploma.entity.ListWork;
import org.diploma.entity.Smeta;
import org.diploma.module.Algorithm;
import org.diploma.module.Work;
import org.diploma.service.GeneralizedWorkService;
import org.diploma.service.SmetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sam.
 */
@Controller
@RequestMapping("/graph")
public class TestController {

    @Autowired
    SmetaService smetaService;

    @Autowired
    GeneralizedWorkService generalizedWorkService;

    private List<Work> works;
    private int allWorkers = 5;
    private Algorithm algorithm;


    @RequestMapping("/{id}")
    public @ResponseBody
    ModelAndView testAlgorithm(@PathVariable(value = "id") Long idSmeta, Authentication authentication) {

        Smeta smeta = smetaService.findById(idSmeta);
        ListWork listWork = smeta.getListWork();
        List<GeneralizedWork> generalizedWorks = generalizedWorkService.findAllByListWork(listWork);

        algorithm = new Algorithm();

        works = fromGenerWorkToSimpleWork(generalizedWorks);
/*
        works.add(new Work(1, "1. Работа 11111", 3, 5, Arrays.asList(-1)));
        works.add(new Work(2, "2 Работа 22222", 3, 3, Arrays.asList(1)));
        works.add(new Work(3, "3 Работа 33333", 1, 5, Arrays.asList(1)));
        works.add(new Work(4, "4 Работа 44444", 2, 4, Arrays.asList(3)));
*/

        algorithm.setWorks(works);
        algorithm.setWorkers(allWorkers);

        ModelAndView modelAndView = new ModelAndView("graph/graph");
//        modelAndView.addObject("smeta", smeta);
        modelAndView.addObject("works", algorithm.calculate());
        return modelAndView;
    }

    private List fromGenerWorkToSimpleWork(List<GeneralizedWork> generalizedWorks) {
        List<Work> works = new ArrayList<>();

        generalizedWorks.forEach(item -> {
            List<String> l = item.getPrevWork() != null
                    ? Arrays.asList(item.getPrevWork().split(","))
                    : new ArrayList<>();
            works.add(new Work(
                    item.getId().intValue(),
                    item.getName(),
                    item.getWorkers().intValue(),
                    item.getDays().intValue(),
                    l
            ));
        });

        return works;
    }

}
