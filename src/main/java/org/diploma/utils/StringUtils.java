package org.diploma.utils;

/**
 * Created by Sam.
 */
public class StringUtils {
    public static boolean isValueNotEmpty(String value){
        return value != null && !value.trim().isEmpty();
    }
}
