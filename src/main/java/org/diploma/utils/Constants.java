package org.diploma.utils;

/**
 * Created by Sam.
 */
public class Constants {

    public static final String DATE_FORMAT_TIME = "HH:mm";
    public static final String DATE_FORMAT_DAY = "dd.MM.yy";
    public static final String DATE_FORMAT = "dd.MM.yyyy HH:mm";
    public static final Long COUNT_DAY_FOR_SCHEDULER = Long.valueOf(20);

}
