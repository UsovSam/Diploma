package org.diploma.utils;

import org.diploma.entity.User;
import org.diploma.security.MyUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by Sam.
 */
public class SecurityContextAdditionalHelper {

    private static final Long SYSTEM_USER_ID = 0L;
    private static final String SYSTEM_USER_LOGIN = "system";
    private static final String SYSTEM_USER_NAME = "Система";

    private static User SYSTEM_USER;

    /**
     * Метод для получения пользователя системы
     * @return пользователь системы
     */
    public static final User getUser(){
        Object principal = SecurityContextHolder.getContext().
                getAuthentication().getPrincipal();
        if (principal instanceof MyUserDetails) {
            return ((MyUserDetails) principal).getUser();
        } else {
            return getSystemUser();
        }
    }

    public static final MyUserDetails getUserDetails() {
        Object principal = SecurityContextHolder.getContext().
                getAuthentication().getPrincipal();
        if (principal instanceof MyUserDetails) {
            return (MyUserDetails) principal;
        } else {
            throw new IllegalStateException("Principal must be MyUserDetails");
        }
    }

    public static final User getSystemUser(){
        if(SYSTEM_USER == null){
            createSystemUser();
        }
        return SYSTEM_USER;
    }

    private static final User createSystemUser(){
        SYSTEM_USER = new User();
        SYSTEM_USER.setId(SYSTEM_USER_ID);
        SYSTEM_USER.setLogin(SYSTEM_USER_LOGIN);
        SYSTEM_USER.setName(SYSTEM_USER_NAME);
        SYSTEM_USER.setSurname("");
        return  SYSTEM_USER;
    }
}
