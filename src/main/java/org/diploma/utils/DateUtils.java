package org.diploma.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Sam.
 */
public class DateUtils {
    private static final DateTimeFormatter ACTIVE_ACCOUNTS_REQUEST_FORMATTER = DateTimeFormatter.ofPattern("yyyy.MM");

    public static boolean isDatePast(LocalDateTime date) {
        return date.isBefore(LocalDateTime.of(LocalDate.now(), LocalTime.MIN));
    }

    public static String getActiveAccountsRequestFormat(LocalDate localDate){
        return (localDate == null) ? "" : localDate.format(ACTIVE_ACCOUNTS_REQUEST_FORMATTER);
    }

    private static String getSimpleDateFormat(LocalDateTime date, String pattern) {
        String returnValue = "";
        if (date != null) {
            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(pattern);
            returnValue = date.format(dateFormat);
        }
        return returnValue;
    }

    public static String getSimpleDateWithTime(LocalDateTime date) {
        return getSimpleDateFormat(date, "dd.MM.yyyy HH:mm:ss");
    }

    public static String getSimpleDate(LocalDateTime date) {
        return getSimpleDateFormat(date, "dd.MM.yyyy");
    }

}
