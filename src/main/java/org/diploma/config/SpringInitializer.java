package org.diploma.config;

import org.diploma.security.SessionListener;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.TimeZone;

/**
 * Created by Sam.
 */
public class SpringInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    /**
     * Метод для установки корневых классов-конфигураций. Должен возвращать
     * {@link AppConfig}.
     *
     * @return массив классов-конфигураций.
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
                AppConfig.class
        };
    }

    /**
     * Метод для установки классов-конфигураций сервлета. Должен возвращаться
     * null, т.к. все необходимые конфигурации уже добавлены к корневому классу
     * конфигурации - {@link AppConfig}.
     *
     * @return массив классов-конфигураций сервлета.
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{
                "/"
        };
    }

    /**
     * Добавляется CharacterEncodingFilter для принудительного перекодирования
     * запросов в UTF-8.
     *
     * @return массив фильтров
     */
    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return new Filter[]{
                characterEncodingFilter
        };
    }

    /**
     * Метод отрабатывает при старте сервлета. В методе устанавливается
     * временная зона в Europe/Moscow
     *
     * @param servletContext контекст сервлета
     * @throws ServletException
     */
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Moscow"));
        super.onStartup(servletContext);
        servletContext.addListener(new SessionListener());
    }

    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        boolean done = registration.setInitParameter("throwExceptionIfNoHandlerFound", "true"); // -> true
        if (!done) {
            throw new RuntimeException();
        }
    }
}
