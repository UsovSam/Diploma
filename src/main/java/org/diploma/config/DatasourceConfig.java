package org.diploma.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by Sam.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.diploma.repository",
        entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "transactionManager")
public class DatasourceConfig {

    @Resource
    private Environment environment;

    private static final String MIGRATIONS_PACKAGE = "org/diploma/db/migrations";
    private static final String ENTITY_PACKAGE = "org.diploma.entity";

    @Bean
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();

        config.setDriverClassName(environment.getRequiredProperty("diploma.datasource.driver"));
        config.setJdbcUrl(environment.getRequiredProperty("diploma.datasource.url"));
        config.setUsername(environment.getRequiredProperty("diploma.datasource.user"));
        config.setPassword(environment.getRequiredProperty("diploma.datasource.password"));

        config.setMaximumPoolSize(Integer.parseInt(environment.getRequiredProperty("diploma.datasource.maximum.pool.size")));

        config.addDataSourceProperty("cachePrepStmts", environment.getRequiredProperty("diploma.datasource.mysql.cachePrepStmts"));
        config.addDataSourceProperty("prepStmtCacheSize", environment.getRequiredProperty("diploma.datasource.mysql.prepStmtCacheSize"));
        config.addDataSourceProperty("prepStmtCacheSqlLimit", environment.getRequiredProperty("diploma.datasource.mysql.prepStmtCacheSqlLimit"));
        config.addDataSourceProperty("useServerPrepStmts", environment.getRequiredProperty("diploma.datasource.mysql.useServerPrepStmts"));

        config.setConnectionTestQuery("SELECT 1");

        return new HikariDataSource(config);
    }

    @Bean(name = "entityManagerFactory")
    @DependsOn(value = "flyway")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setPackagesToScan(ENTITY_PACKAGE); // пакет, в котором искать сущности
        entityManagerFactoryBean.setDataSource(dataSource);
        HibernatePersistenceProvider hProvider = new HibernatePersistenceProvider();
        entityManagerFactoryBean.setPersistenceProvider(hProvider);
        Properties jpaProperties = new Properties();
        jpaProperties.setProperty("hibernate.dialect", environment.getRequiredProperty("diploma.datasource.hibernate.dialect"));
        jpaProperties.setProperty("hibernate.show_sql", environment.getRequiredProperty("diploma.datasource.hibernate.show_sql"));
        jpaProperties.setProperty("hibernate.connection.characterEncoding", "UTF-8");
        jpaProperties.setProperty("hibernate.enable_lazy_load_no_trans", "true");
        jpaProperties.setProperty("hibernate.jdbc.batch_size", "20");
        jpaProperties.setProperty("hibernate.order_inserts","true");
        jpaProperties.setProperty("hibernate.order_updates","true");
        jpaProperties.setProperty("hibernate.jdbc.fetch_size","400");
        jpaProperties.setProperty("hibernate.jdbc.batch_versioned_data","true");
        entityManagerFactoryBean.setJpaProperties(jpaProperties);
        return entityManagerFactoryBean;
    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory(dataSource).getObject());
        return transactionManager;
    }

    @Bean(initMethod = "migrate")
    public Flyway flyway(DataSource dataSource) {
        Flyway flyway = new Flyway();
        flyway.setBaselineOnMigrate(true);
        flyway.setLocations(MIGRATIONS_PACKAGE);
        flyway.setDataSource(dataSource);
        return flyway;
    }

}
