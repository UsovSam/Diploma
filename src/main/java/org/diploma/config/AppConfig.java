package org.diploma.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import javax.validation.Validator;

/**
 * Created by Sam.
 */
@Configuration
@ComponentScan(basePackages
        =
        {
                "org.diploma.service",
                "org.diploma.security",
                "org.diploma.utils"
        })
@Import(
        {
                PropertyConfig.class,  DatasourceConfig.class,
                SecurityConfig.class, WebConfig.class
        })
@EnableScheduling
@EnableAsync
public class AppConfig {


        @Bean
        public MethodValidationPostProcessor methodValidationPostProcessor() {

                MethodValidationPostProcessor processor =
                        new MethodValidationPostProcessor();
                processor.setValidator(validator());
                return processor;
        }

        @Bean
        public Validator validator() {
                return new LocalValidatorFactoryBean();
        }

}
