/**
 * Created by Sam on 31.05.2017.
 */

window.onload = function () {

    var tbodyGenerWork = $('#tbodyGenerWork'), collectSelect = $('#collect-input'),
        groupSelect = $('#group-input'), workInput = $('#work-input'),
        tbodyStandart = $('#tbodyStandart'), modalBodyTable = $('#modalBodyTable');

    var $generWorkTr = undefined;
    var $standartWorkTr = undefined;
    var idStandart, idWork;

    getGenerWork();

    getCollect();

    $('#btn-input').on('click', '', function (e) {
        var collect = collectSelect.find("option:selected").attr('value');
        var group = groupSelect.find("option:selected").attr('value');
        var work = workInput.val();
        console.log(collect, group, work);
        $.ajax({
            type: "POST",
            url: contextRoot + "findstandart/json",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data: {"collect": collect, "group": group, "work": work},
            success: function (data) {
                console.log(data);
                setStandarts(data);
            }
        });


    });

    $('#tbodyGenerWork').on('click', 'tr', function (e) {
        if ($generWorkTr !== undefined)
            $generWorkTr.css("background-color", "white");
        var $this = $(this), text = $this.find('td').first().text();
        $this.css("background-color", "red");
        $generWorkTr = $this;
        workInput.val(text);
        idWork = $this.attr('id_work');
    });

    $('#tbodyStandart').on('click', 'tr', function (e) {
        if ($standartWorkTr !== undefined)
            $standartWorkTr.css("background-color", "white");
        var $this = $(this), id_standart = $this.attr('id_standart');
        $this.css("background-color", "blue");
        $standartWorkTr = $this;
        idStandart = id_standart;
        fillModal(id_standart);
    });

    $(".chosen-select").chosen({
        allow_single_deselect: true,
        width: "100%"
    });

    collectSelect.change(function () {
        var collect = collectSelect.find("option:selected");
        groupSelect.empty();
        $.get(contextRoot + "group/json/" + collect.attr("value"), function (response) {
            groupSelect
                .append($("<option></option>")
                    .attr("value", "")
                    .text(""));
            $.each(response, function (index, item) {
                groupSelect
                    .append($("<option></option>")
                        .attr("value", item.id)
                        .text(item.name));
            });
            groupSelect.trigger("chosen:updated");
        });
    });

    $('#btnModalOk').click(function (e) {
        $.ajax({
            type: "POST",
            url: contextRoot + "setstandart/json",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data: {"idWork": idWork, "idStandart": idStandart},
            success: function (data) {
                console.log(data);
                $('#myModal').modal('hide');
            }
        });
    });

    function getGenerWork() {
        $.get(contextRoot + "generwork/json/" + idListWork, function (data) {
            $.each(data, function (index, item) {
                tbodyGenerWork.append("<tr id_work=\"" + item.id + "\">" +
                    "<td>" + item.name + "</td>" +
                    "<td>" + item.value + " (" + item.units + ")</td>" +
                    "<td>" + item.normative + "</td>" +
                    "</tr>");
            });
        });
    }

    function getCollect() {
        $.get(contextRoot + "collections/json", function (response) {
            console.log(response);
            collectSelect
                .append($("<option></option>")
                    .attr("value", "")
                    .text(""));
            $.each(response, function (index, item) {
                collectSelect
                    .append($("<option></option>")
                        .attr("value", item.id)
                        .text(item.name));
            });
            collectSelect.trigger("chosen:updated");
        });
    }

    function setStandarts(data) {
        tbodyStandart.empty();
        $.each(data, function (index, item) {
            tbodyStandart.append("<tr id_standart=\"" + item.id + "\">" +
                "<td class='col-xs-3'>" + item.code + "</td>" +
                "<td>" + item.name + "</td>" +
                "</tr>");
        });
    }

    function fillModal(id) {
        modalBodyTable.empty();
        $.ajax({
            type: "GET",
            url: contextRoot + "findstandart/" + id + "/json",
            success: function (data) {
                $.each(data.resources, function (index, item) {
                    var obj = {};
                    if (item.directoryResource.type == 2) {
                        obj = item.directoryResource.directoryCharacteristic;
                    } else {
                        if (item.directoryResource.type == 1) {
                            obj = item.directoryResource.directoryMachine;
                        } else {
                            obj = item.directoryResource.directoryMaterial;
                        }
                    }
                    modalBodyTable.append("<tr>" +
                        "<td>" + item.directoryResource.code + "</td>" +
                        "<td>" + obj.name + "</td>" +
                        "<td>" + obj.units.name + "</td>" +
                        "<td>" + item.value + "</td>" +
                        "</tr>");

                });
                $('#myModal').modal('show');
            }
        });
    }

};
